// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTA_ProjectACharacter_generated_h
#error "ProjectACharacter.generated.h already included, missing '#pragma once' in ProjectACharacter.h"
#endif
#define PROJECTA_ProjectACharacter_generated_h

#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_SPARSE_DATA
#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_RPC_WRAPPERS
#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectACharacter(); \
	friend struct Z_Construct_UClass_AProjectACharacter_Statics; \
public: \
	DECLARE_CLASS(AProjectACharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectA"), NO_API) \
	DECLARE_SERIALIZER(AProjectACharacter)


#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAProjectACharacter(); \
	friend struct Z_Construct_UClass_AProjectACharacter_Statics; \
public: \
	DECLARE_CLASS(AProjectACharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectA"), NO_API) \
	DECLARE_SERIALIZER(AProjectACharacter)


#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectACharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectACharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectACharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectACharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectACharacter(AProjectACharacter&&); \
	NO_API AProjectACharacter(const AProjectACharacter&); \
public:


#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectACharacter(AProjectACharacter&&); \
	NO_API AProjectACharacter(const AProjectACharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectACharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectACharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectACharacter)


#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AProjectACharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AProjectACharacter, FollowCamera); }


#define ProjectA_Source_ProjectA_ProjectACharacter_h_9_PROLOG
#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_SPARSE_DATA \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_RPC_WRAPPERS \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_INCLASS \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectA_Source_ProjectA_ProjectACharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_SPARSE_DATA \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_INCLASS_NO_PURE_DECLS \
	ProjectA_Source_ProjectA_ProjectACharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTA_API UClass* StaticClass<class AProjectACharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectA_Source_ProjectA_ProjectACharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
