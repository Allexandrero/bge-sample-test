// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTA_ProjectAGameMode_generated_h
#error "ProjectAGameMode.generated.h already included, missing '#pragma once' in ProjectAGameMode.h"
#endif
#define PROJECTA_ProjectAGameMode_generated_h

#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_SPARSE_DATA
#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_RPC_WRAPPERS
#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectAGameMode(); \
	friend struct Z_Construct_UClass_AProjectAGameMode_Statics; \
public: \
	DECLARE_CLASS(AProjectAGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectA"), PROJECTA_API) \
	DECLARE_SERIALIZER(AProjectAGameMode)


#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAProjectAGameMode(); \
	friend struct Z_Construct_UClass_AProjectAGameMode_Statics; \
public: \
	DECLARE_CLASS(AProjectAGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectA"), PROJECTA_API) \
	DECLARE_SERIALIZER(AProjectAGameMode)


#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PROJECTA_API AProjectAGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectAGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PROJECTA_API, AProjectAGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectAGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PROJECTA_API AProjectAGameMode(AProjectAGameMode&&); \
	PROJECTA_API AProjectAGameMode(const AProjectAGameMode&); \
public:


#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PROJECTA_API AProjectAGameMode(AProjectAGameMode&&); \
	PROJECTA_API AProjectAGameMode(const AProjectAGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PROJECTA_API, AProjectAGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectAGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectAGameMode)


#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define ProjectA_Source_ProjectA_ProjectAGameMode_h_9_PROLOG
#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_SPARSE_DATA \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_RPC_WRAPPERS \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_INCLASS \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectA_Source_ProjectA_ProjectAGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_SPARSE_DATA \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_INCLASS_NO_PURE_DECLS \
	ProjectA_Source_ProjectA_ProjectAGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTA_API UClass* StaticClass<class AProjectAGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectA_Source_ProjectA_ProjectAGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
